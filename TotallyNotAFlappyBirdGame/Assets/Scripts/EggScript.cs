﻿using UnityEngine;
using System.Collections;

public class EggScript : MovingElementBase
{
    new void FixedUpdate()
    {
        base.FixedUpdate();
        transform.position += new Vector3(GameController.Get.ActualSpeed / 2, 0, 0) * Time.deltaTime;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "Player")
            return;

        if (coll.gameObject.tag == "Enemy")
        {
            coll.gameObject.GetComponent<EnemyScript>().OnEggHit();
        }
        if (coll.gameObject.tag == "Girl")
        {
            coll.gameObject.GetComponent<GirlScript>().OnEggHit();
        }

        GameObject brokenEggParticle = Instantiate(PrefabsHelper.Get.BrokenEggParticlePrefab, PrefabsHelper.Get.ParticlesTransform);
        brokenEggParticle.transform.position = transform.position;
        brokenEggParticle.GetComponent<ParticleSystem>().Play();
        Destroy(gameObject);
    }
}
