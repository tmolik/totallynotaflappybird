﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUiController : MonoBehaviour
{
    public static GameUiController Get { get; private set; }

    public GameUiController()
    {
        Get = this;
    }


    public Text EggCountText;

    public Text ScoreText;

    public RectTransform InfoTextRect;


    public void UpdateEggCountText()
    {
        EggCountText.text = GameController.Get.AvailableEggs.ToString();
    }

    public void UpdateScoreText()
    {
        ScoreText.text = GameController.Get.Score.ToString();
    }
}
