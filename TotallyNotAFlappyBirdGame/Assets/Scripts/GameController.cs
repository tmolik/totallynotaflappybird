﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static GameController Get { get; private set; }

    public GameController()
    {
        Get = this;
    }

    /// <summary>
    /// Amount of carried eggs possible to drop
    /// </summary>
    internal int AvailableEggs;

    /// <summary>
    /// Current score   
    /// </summary>
    internal int Score;

    /// <summary>
    /// Reference to the pipe object that was last created.
    /// </summary>
    public GameObject LastCreatedPipe;

    /// <summary>
    /// Is game world running? (are pipes moving etc?)
    /// </summary>
    internal bool IsRunning = false;

    /// <summary>
    /// Is game lost 
    /// </summary>
    internal bool IsLost = false;

    /// <summary>
    /// How fast is world moving
    /// </summary>
    internal float ActualSpeed;

    /// <summary>
    /// At what score value will world speed increase
    /// </summary>
    private float NextScoreValueToChangeSpeed;

    internal List<MovingElementBase> MovingElements = new List<MovingElementBase>();

    // Use this for initialization
    void Start()
    {
        Score = 0;
        NextScoreValueToChangeSpeed = 30;

        ActualSpeed = 4f;

        AvailableEggs = 0;
        GameUiController.Get.UpdateEggCountText();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (!IsRunning)
        {
            if (Input.GetKey(KeyCode.Space) || Input.touchCount > 0)
            {
                SetWorldMoving(true);
                IsRunning = true;

                GameUiController.Get.InfoTextRect.gameObject.SetActive(false);
            }
        }

        if (IsLost)
        {
            if ((Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.RightShift)) || Input.touchCount > 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    /// <summary>
    /// Add score, change speed if it score reached another speed level
    /// </summary>
    /// <param name="value"></param>
    public void AddScore(int value)
    {
        Score += value;
        if (Score > NextScoreValueToChangeSpeed)
        {
            ActualSpeed += .1f;
            NextScoreValueToChangeSpeed += 30;
        }
    }

    /// <summary>
    /// Invoked when player crashes and loses game, it stops world 
    /// </summary>
    public void StopGame()
    {
        SetWorldMoving(false);
        IsLost = true;
        GameUiController.Get.UpdateScoreText();
        GameUiController.Get.InfoTextRect.gameObject.SetActive(true);
    }

    /// <summary>
    /// Funkcja do wl/wylaczania wszystkich skryptow poruszajacych sie obiektow zeby przestaly sie rusac
    /// </summary>
    public void SetWorldMoving(bool isWorldMoving)
    {
        foreach (MovingElementBase movingElement in MovingElements)
        {
            if (movingElement == null)
                continue;
            movingElement.IsMoving = isWorldMoving;
        }
        BirdMove.Get.IsMoving = isWorldMoving;
    }

    /// <summary>
    /// Generates another world fragment, another pipe, egg to collect and mob
    /// </summary>
    public void GenerateNextWorldFragment()
    {
        GameObject newPipe = Instantiate(PrefabsHelper.Get.PipePrefab, new Vector2(LastCreatedPipe.transform.position.x + 7, Random.Range(-0.9f, 2.95f)), Quaternion.identity);
        newPipe.transform.SetParent(PrefabsHelper.Get.PipesTransform);
        LastCreatedPipe = newPipe;
        newPipe.GetComponent<MovingElementBase>().IsMoving = true;

        GameObject eggFind = Instantiate(PrefabsHelper.Get.CollectableEggPrefab, new Vector2(newPipe.transform.position.x, Random.Range(newPipe.transform.position.y - 1.6f, newPipe.transform.position.y + 1.6f)), Quaternion.identity);
        eggFind.transform.SetParent(PrefabsHelper.Get.EggsTransform);
        eggFind.GetComponent<CollectableEggController>().IsMoving = true;

        GenerateNextMob();
    }

    /// <summary>
    /// Funckja do tworzenia kolejnego wroga lub dziewczyny pomiedzy rurami
    /// </summary>
    private void GenerateNextMob()
    {
        if (Random.Range(0, 100) > 70)
        {
            GameObject girl = Instantiate(PrefabsHelper.Get.GirlPrefab, new Vector2(LastCreatedPipe.transform.position.x + Random.Range(2, 5), -4.6f), Quaternion.identity);
            girl.transform.SetParent(PrefabsHelper.Get.EnemiesAndGirlsTransform);
            girl.GetComponent<GirlScript>().IsMoving = true;
        }
        else
        {
            GameObject enemyToSpawn = Random.Range(0, 100) > 50 ? PrefabsHelper.Get.Enemy1Prefab : PrefabsHelper.Get.Enemy2Prefab;
            GameObject enemy = Instantiate(enemyToSpawn, new Vector2(LastCreatedPipe.transform.position.x + Random.Range(2, 5), -4.53f), Quaternion.identity);
            enemy.transform.SetParent(PrefabsHelper.Get.EnemiesAndGirlsTransform);
            enemy.GetComponent<EnemyScript>().IsMoving = true;
        }

    }

}
