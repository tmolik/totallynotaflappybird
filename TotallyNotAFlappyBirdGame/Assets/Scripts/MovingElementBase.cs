﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingElementBase : MonoBehaviour
{
    public bool IsMoving = false;

    public void Start()
    {
        if (!GameController.Get.MovingElements.Contains(this))
            GameController.Get.MovingElements.Add(this);
    }

    public void FixedUpdate()
    {
        if (!IsMoving)
            return;

        transform.position += new Vector3(-1 * GameController.Get.ActualSpeed, 0, 0) * Time.deltaTime;

        if (transform.position.x < -20)
        {
            GameController.Get.MovingElements.Remove(this);
            Destroy(gameObject);
        }
    }
}
