﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing an egg to collect by player
/// </summary>
public class CollectableEggController : MovingElementBase
{
    new void FixedUpdate()
    {
        base.FixedUpdate();

        float newYPosition = Mathf.Sin(Time.time) * 0.5f * Time.deltaTime;
        //Debug.Log(newYPosition);
        transform.position += new Vector3(0, newYPosition, 0);
    }

    /// <summary>
    /// Invoked when player collected egg, instantiates particle and destroys this egg.
    /// </summary>
	public void PlayParticleAndDestroy()
    {
        GameObject eggFindParticle = Instantiate(PrefabsHelper.Get.CollectedEggParticlePrefab, PrefabsHelper.Get.ParticlesTransform );
        eggFindParticle.transform.position = transform.position;
        eggFindParticle.GetComponent<ParticleSystem>().Play();
        Destroy(gameObject);
    }
}
