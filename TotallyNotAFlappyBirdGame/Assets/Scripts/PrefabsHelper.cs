﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabsHelper : MonoBehaviour
{

    public static PrefabsHelper Get { get; private set; }


    #region === Objects ===
    [Header("Objects")]
    public GameObject GirlPrefab;
    public GameObject Enemy1Prefab;
    public GameObject Enemy2Prefab;

    public GameObject CollectableEggPrefab;
    public GameObject PipePrefab;
    public GameObject EggPrefab;

    #endregion

    #region === Particles ===
    [Header("Particles")]

    public GameObject BrokenEggParticlePrefab;
    public GameObject CollectedEggParticlePrefab;
    public GameObject BloodParticle;
    #endregion

    #region === Parent Transforms ===
    [Header("Parent Transforms")]

    public Transform PipesTransform;
    public Transform EnemiesAndGirlsTransform;
    public Transform EggsTransform;
    public Transform ParticlesTransform;
    #endregion

    public PrefabsHelper()
    {
        Get = this;
    }
}
