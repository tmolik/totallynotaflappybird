﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BirdCollisions : MonoBehaviour
{

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Egg" )
        {
            return;
        }
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyScript>().OnEggHit();
        }

        GameController.Get.StopGame();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "between")
        {
            coll.GetComponent<BoxCollider2D>().enabled = false; 
            GameController.Get.GenerateNextWorldFragment();
            GameController.Get.AddScore(1);
            GameUiController.Get.UpdateScoreText();
        }

        if (coll.gameObject.tag == "EggFind")
        {
            GameController.Get.AvailableEggs = Mathf.Min(9, GameController.Get.AvailableEggs + 1);
            GameUiController.Get.UpdateEggCountText();
            coll.gameObject.GetComponent<CollectableEggController>().PlayParticleAndDestroy();
        }
    }

}
