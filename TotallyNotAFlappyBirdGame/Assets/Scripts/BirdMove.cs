﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class BirdMove : MonoBehaviour
{
    public static BirdMove Get;

    public BirdMove()
    {
        Get = this;
    }

    Vector3 velocity = Vector3.zero;
    private Vector3 gravity = new Vector3(0, -15, 0);
    private Vector3 flapVelocity = new Vector3(0, 7, 0);
    private float maxSpeed = 7f;
    bool didFlap = false;


    public bool IsMoving = false;

    // Use this for initialization
    void Start()
    {
        velocity = new Vector3(0, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsMoving)
            return;

        if (Input.GetKeyDown(KeyCode.Space) || (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began))
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                {
                    didFlap = true;
                }
            }
            else
            {
                didFlap = true;
            }

        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            ReleaseEgg();
        }
    }

    public void ReleaseEgg()
    {
        if (GameController.Get.AvailableEggs > 0)
        {
            float  eggY = velocity.y >= 0 ? 0.9f : 1.5f;

            GameObject egg = Instantiate(PrefabsHelper.Get.EggPrefab, new Vector2(transform.position.x, transform.position.y - eggY), Quaternion.identity);
            egg.GetComponent<EggScript>().IsMoving = true;
            GameController.Get.AvailableEggs--;
            GameUiController.Get.UpdateEggCountText();

            transform.position = new Vector2(-5f, transform.position.y);
        }
    }

    void FixedUpdate()
    {
        if (!IsMoving)
            return;

        velocity += gravity * Time.deltaTime;

        //Obsluga podskoku
        if (didFlap)
        {
            if (velocity.y < 0)
                velocity.y = 0;
            velocity += flapVelocity;
            didFlap = false;
        }

        velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

        transform.position += velocity * Time.deltaTime;

        //Manipulacja rotacją
        float angle = 0;
        if (velocity.y < 0)
        {
            angle = Mathf.Lerp(0, -60, -velocity.y / maxSpeed);
        }
        if (velocity.y > 0)
        {
            angle = Mathf.Lerp(0, 45, velocity.y / maxSpeed);
        }
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }

}
