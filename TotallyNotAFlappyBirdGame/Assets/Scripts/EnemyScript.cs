﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MovingElementBase
{

    private float direction;
    private float enemySpeed;

    // Use this for initialization
    public new void Start()
    {
        base.Start();
        direction = Random.Range(0, 10) > 5 ? -1 : 1;
        enemySpeed = Random.Range(0.5f, 2f);
    }

    new void FixedUpdate()
    {
        base.FixedUpdate();
        transform.position += new Vector3(direction * enemySpeed, 0, 0) * Time.deltaTime;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "bottom pipe")
        {
            direction *= -1;
            transform.rotation = Quaternion.Euler(0, direction == -1 ? 180 : 0, 0);
        }
    }



	public void OnEggHit()
    {
        AudioController.Get.PlayEnemyKilled();

        GameObject bloodParticle = Instantiate(PrefabsHelper.Get.BloodParticle, PrefabsHelper.Get.ParticlesTransform);
        bloodParticle.transform.position = transform.position;
        bloodParticle.GetComponent<ParticleSystem>().Play();

        GameController.Get.AddScore(3);
        GameUiController.Get.UpdateScoreText();

        Destroy(gameObject);
    }
}
