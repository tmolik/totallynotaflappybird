﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController Get { get; private set; }

    public AudioController()
    {
        Get =this;
    }

    public AudioSource MainAudioSource;

    public AudioClip BackgroundMusicClip;
    public AudioClip EnemyKilledClip;
    public AudioClip GirlKilledClip;


    // Use this for initialization
    void Start()
    {
        MainAudioSource.clip = BackgroundMusicClip;
        MainAudioSource.Play();
    }

    public void PlayEnemyKilled()
    {
        MainAudioSource.PlayOneShot(EnemyKilledClip);
    }

    public void PlayGirlKilled()
    {
        MainAudioSource.PlayOneShot(GirlKilledClip);
    }

    public void ChangeAudioVolume(bool mute)
    {
        MainAudioSource.volume = mute ? 0 : 1;
    }

}
